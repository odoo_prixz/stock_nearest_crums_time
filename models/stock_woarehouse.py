from odoo import fields, models


class StockWarehouse(models.Model):
    _inherit = 'stock.warehouse'

    date_from = fields.Float(string="Desde", )
    date_to = fields.Float(string="Hasta", )
    red_fria = fields.Boolean(string="CRUM para red fría", )
    is_controlado = fields.Boolean(string="CRUM para productos controlados", )
    days_works = fields.Many2many("week.days", string="Días laborales", )
    always_work = fields.Boolean(string="24 horas", default=True)
