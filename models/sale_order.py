from odoo.tools.safe_eval import pytz

from odoo import models, fields
from datetime import timedelta


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    def get_zip_ids(self, partner_shipping_id):
        ids = []
        zip_ids = super(SaleOrder, self).get_zip_ids(partner_shipping_id)
        today = self.get_day_now()
        dia = today.isoweekday()
        hora = timedelta(hours=today.hour, minutes=today.minute)
        for zip_ in zip_ids:
            dias_laborales = zip_.warehouse_id.mapped("days_works.code")
            if dia not in dias_laborales:
                continue
            if not zip_.warehouse_id.always_work:
                desde = timedelta(hours=zip_.warehouse_id.date_from)
                hasta = timedelta(hours=zip_.warehouse_id.date_to)
                if desde <= hora <= hasta:
                    ids.append(zip_.id)
                continue
            ids.append(zip_.id)
        new_ids = self.env["stock.zip.range"].browse(ids)
        return new_ids

    def get_ware_house_ids(self, partner_shipping_id):
        warehouse_ids = super(SaleOrder, self).get_ware_house_ids(partner_shipping_id)
        ids = []
        today = self.get_day_now()
        dia = today.isoweekday()
        hora = timedelta(hours=today.hour, minutes=today.minute)
        for warehouse_id in warehouse_ids:
            dias_laborales = warehouse_id.mapped("days_works.code")
            if dia not in dias_laborales:
                continue
            if not warehouse_id.always_work:
                desde = timedelta(hours=warehouse_id.date_from)
                hasta = timedelta(hours=warehouse_id.date_to)
                if desde <= hora <= hasta:
                    ids.append(warehouse_id.id)
                continue
            ids.append(warehouse_id.id)
        new_warehouse_ids = self.env["stock.warehouse"].browse(ids)
        return new_warehouse_ids

    def splits_orden_por_crums(self, partner_shipping_id, split_max):
        zip_ids = self.get_zip_ids(partner_shipping_id)
        warehouse_ids = self.get_ware_house_ids(partner_shipping_id)
        split_real = 0
        warehouse_split = self.env["stock.warehouse"]
        for line in self.order_line.filtered(lambda x: x.product_id.type in ['product']):
            if line.product_id.fraccion in (2, 3):
                warehouse_ids_receta = warehouse_ids.filtered(lambda x: x.is_controlado)
                zip_receta = zip_ids.filtered(lambda x: x.warehouse_id.is_controlado)
                split_real, warehouse_split = line.split_line_for_crums(zip_receta, warehouse_ids_receta, split_max, split_real, warehouse_split)
            elif line.Sale_Refrigerado:
                zip_red_fria = zip_ids.filtered(lambda x: x.warehouse_id.red_fria)
                warehouse_ids_red_fria = warehouse_ids.filtered(lambda x: x.red_fria)
                split_real, warehouse_split = line.split_line_for_crums(zip_red_fria, warehouse_ids_red_fria, split_max, split_real, warehouse_split)
            else:
                split_real, warehouse_split = line.split_line_for_crums(zip_ids, warehouse_ids, split_max, split_real, warehouse_split)

    def get_day_now(self):
        today = fields.datetime.now().astimezone(pytz.timezone(self.env.user.tz or 'UTC'))
        return today

    def group_by_best_crum(self):
        res = None
        for record in self:
            zip_nuevos, warehouse_nuevos = record.get_crums_refri_contro()
            lines_warehouses = record.order_line.mapped("line_warehouse_id")
            if zip_nuevos and warehouse_nuevos:
                best_warehouse = record.get_best_crum_refri_control(zip_nuevos, warehouse_nuevos, lines_warehouses)
                if best_warehouse:
                    for line in record.order_line.filtered(lambda x: x.product_id.type in ['product']):
                        line.line_warehouse_id = best_warehouse.id
            else:
                res = super(SaleOrder, self).group_by_best_crum()
        return res

    def get_best_crum_refri_control(self, zip_warehouse, state_warehouse, lines_warehouses):
        for zip_id in zip_warehouse:
            if zip_id.id in lines_warehouses.ids:
                return zip_id
        for state_id in state_warehouse:
            if state_id.id in lines_warehouses.ids:
                return state_id
        return False

    def get_crums_refri_contro(self):
        zip_nuevos = warehouse_nuevos = None
        partner_shipping_id = self.partner_shipping_id
        zip_ids = self.get_zip_ids(partner_shipping_id)
        warehouse_ids = self.get_ware_house_ids(partner_shipping_id)
        products_refri = self.order_line.filtered(lambda x: x.product_id.type in ['product']).filtered(
            lambda x: x.Sale_Refrigerado)
        products_controlados = self.order_line.filtered(lambda x: x.product_id.type in ['product']).filtered(
            lambda x: x.product_id.fraccion in (2, 3))
        if products_controlados and products_refri:
            zip_nuevos = zip_ids.filtered(lambda x: x.warehouse_id.red_fria and x.is_controlado)
            warehouse_nuevos = warehouse_ids.filtered(lambda x: x.red_fria and x.is_controlado)
            return zip_nuevos, warehouse_nuevos
        if products_refri:
            zip_nuevos = zip_ids.filtered(lambda x: x.warehouse_id.red_fria)
            warehouse_nuevos = warehouse_ids.filtered(lambda x: x.red_fria)
        if products_controlados:
            warehouse_nuevos = warehouse_ids.filtered(lambda x: x.is_controlado)
            zip_nuevos = zip_ids.filtered(lambda x: x.warehouse_id.is_controlado)
        return zip_nuevos, warehouse_nuevos