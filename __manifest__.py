{
    'name': 'Horarios en crums',
    'version': '14.0.1.0.0',
    'category': 'sale',
    'summery': 'Horarios en crum',
    'description': 'Se dan horarios de atencion en crums y se asignan crums solo despachados para receta y red fria',
    'author': '',
    'depends': [
        'woocommerce_odoo_connector',
        'stock_nearest_warehouse_acs',
        'stock_nearest_crums_woo_split',
        'red-fria-receta'
    ],
    'data': [
        "security/ir.model.access.csv",
        "data/ir_data_week_days.xml",
        "views/stock_warehouse_views.xml",
    ],
    'license': "LGPL-3",
    'installable': True,
    'application': False,
}
